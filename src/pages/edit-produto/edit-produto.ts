import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProdutoProvider } from '../../providers/produto/produto';

@IonicPage()
@Component({
  selector: 'page-edit-produto',
  templateUrl: 'edit-produto.html',
})
export class EditProdutoPage {
  form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, private toast: ToastController,
    private produtoProvider: ProdutoProvider) {

    let produto = {
      categoria: '1',
      tamanho: 'p',
      ativo: true
    }

    if (this.navParams.data.produto) {
      produto = this.navParams.data.produto;
    }

    this.createForm(produto);
  }

  private createForm(produto: any) {
    this.form = this.formBuilder.group({
      key: [produto.key],
      nome: [produto.nome, [Validators.required, Validators.minLength(2)]],
      descricao: [produto.descricao, [Validators.required, Validators.minLength(2)]],
      categoria: [produto.categoria],
      preco: [produto.preco, [Validators.required]],
      dataCadastro: [produto.dataCadastro, [Validators.required]],
      tamanho: [produto.tamanho],
      ativo: [produto.ativo]
    })
  }

  salvar() {
    if (this.form.valid) {
      this.produtoProvider.save(this.form.value)
        .then(() => {
          this.toast.create({ message: 'Produto salvo com sucesso', duration: 1000 }).present();
          this.navCtrl.pop(); // fechando a tela
        });
    }
  }

}
