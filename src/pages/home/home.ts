import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { ProdutoProvider } from '../../providers/produto/produto';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  produtos: Observable<any>;

  constructor(public navCtrl: NavController, private produtoProvider: ProdutoProvider,
    public toast: ToastController, public alert: AlertController) {
  }

  addProduto() {
    this.navCtrl.push('EditProdutoPage');
  }

  ionViewDidLoad() {
    this.produtos = this.produtoProvider.getAll();
  }

  filtrarProdutos(evt: any) {
    if (evt && evt.target.value) {
      this.produtos = this.produtoProvider.filtrarPorNome(evt.target.value);
    } else {
      this.produtos = this.produtoProvider.getAll();
    }
  }

  getCategoriaDescricao(valor: number) {
    return this.produtoProvider.getCategoriaDescricao(valor);
  }

  getTamanhoDescricao(valor: string) {
    return this.produtoProvider.getTamanhoDescricao(valor);
  }

  editProduto(produto: any) {
    this.navCtrl.push('EditProdutoPage', { produto: produto });
  }

  remover(key: string, nome: string) {
    this.alert.create({
      title: 'Excluir produto : ' + nome,
      subTitle: 'Essa operação não poderá ser desfeita.',
      message: 'Confirma a exclusão do produto?',
      buttons: [
        {
          text: 'Remover',
          handler: () => {
            this.removerProduto(key);
          }
        },
        {
          text: 'Cancelar'
        }
      ]
    }).present();
  }

  removerProduto(key: string) {
    this.produtoProvider.remove(key)
      .then(() => {
        this.toast.create({ message: 'Produto removido.', duration: 2000 }).present();
      });
  }
}
