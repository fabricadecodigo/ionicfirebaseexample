import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ProdutoProvider {
  PATH = 'produtos/'

  constructor(public db: AngularFireDatabase) { }

  save(produtoData: any) {
    return new Promise((resolve, reject) => {
      const produtoRef = this.db.list(this.PATH);

      let produto = {
        nome: produtoData.nome,
        descricao: produtoData.descricao,
        categoria: produtoData.categoria,
        preco: produtoData.preco,
        dataCadastro: produtoData.dataCadastro,
        tamanho: produtoData.tamanho,
        ativo: produtoData.ativo
      }

      if (produtoData.key) {
        produtoRef.update(produtoData.key, produto)
          .then(() => resolve())
          .catch(() => reject());
      } else {
        produtoRef.push(produto)
          .then(() => resolve());
      }
    });
  }

  getAll() {
    return this.db.list(this.PATH, ref => ref.orderByChild('nome'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
  }

  filtrarPorNome(nome: string) {
    // na query abaixo é como se eu estivesse fazendo a seguinte query no SQL normal
    // select * from produtos where nome like 'Teste a%'
    // ou seja, estou pegando todos os produtos em que o nome começa com "Teste a"
    // no firebase precisamos fazer o startAt junto com o endAt para resover problemas de encode
    // se você fizer somente startAt o filtro não vai funcionar
    return this.db.list(this.PATH, ref => ref.orderByChild('nome').startAt(nome).endAt(nome + '\uf8ff'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
  }

  getCategoriaDescricao(valor: number) {
    if (valor == 1) {
      return 'Hambúrguer';
    } else if (valor == 2) {
      return 'Refrigerante';
    } else if (valor == 3) {
      return 'Batata';
    }
  }

  getTamanhoDescricao(valor: string) {
    if (valor == 'p') {
      return 'Pequeno';
    } else if (valor == 'm') {
      return 'Médio';
    } else if (valor == 'g') {
      return 'Grande';
    }
  }

  remove(key: string) {
    return this.db.list(this.PATH).remove(key);
  }
}
